GuiFont! Fira Code:h10
call GuiMousehide(v:false)
GuiTabline 0

function! s:GuiResetFont() abort
  let l:keep = g:GuiFont
  let l:tmp = substitute(g:GuiFont, ':h\d\+', ':h20', '')
  execute 'GuiFont! ' . l:tmp
  execute 'GuiFont! ' . l:keep
endfunction

function! s:GuiFontSize(...) abort
  let l:gui_size = substitute(g:GuiFont, '.\+:h', '', '')
  if a:0 == 0 
    echom 'Font size: ' . l:gui_size
    return
  endif
  if a:0 == 1
    if a:1[0] ==# '+' || a:1[0] ==# '-'
      let l:gui_size += str2nr(len(a:1) > 1 ? a:1 : a:1 . '2')
    else
      let l:gui_size = str2nr(a:1)
    endif
    execute 'GuiFont! ' . substitute(g:GuiFont, ':h\d\+', ':h' . string(l:gui_size), '')
  endif
endfunction
command! -nargs=? GuiFontSize call s:GuiFontSize(<f-args>)

let g:gui_loaded = 1
