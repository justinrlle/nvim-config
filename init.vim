"=============================================================================
" FILE: init.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

scriptencoding utf8

"
" My init.vim. It supports only neovim, even though some features could be
" checked for vim8 use.
"
" See the README.md for installation instructions
"

let g:allowed_options = [
      \ 'path'
      \ ]

" Set the runtime path to the directory of this init file, instead of the
" standard one (~/.config/nvim on unixes, for example).
" {{{
let g:init_path = expand('<sfile>:p:h')
let s:standard_path = expand(has('win32') ? '~/AppData/Roaming/nvim' : '~/.config/nvim')
if g:init_path !=# s:standard_path
  execute 'set runtimepath-=' . s:standard_path
  execute 'set runtimepath-=' . s:standard_path . '/after'
  let &runtimepath = g:init_path . ',' . &runtimepath . ',' . g:init_path . '/after'
endif
let g:init_rtp = split(&runtimepath, ',')
" }}}

" Load the feature files.
" {{{
try
  call self#features#_Load(expand(g:init_path . '/default-features.toml'))
catch /^features/
  echo 'no `default-features.toml` file in installation directory'
endtry
try
  call self#features#_Load(expand(g:init_path . '/features.toml'))
catch /^features/
endtry
call self#features#LoadOptions()

" will replace complicated 'features.toml'
let g:workspace_config = {}
let s:file_path = findfile('vim.json', getcwd() . '/.config')
try
  let g:workspace_config = json_decode(readfile(s:file_path))
catch
endtry
call self#log#({'wc': g:workspace_config})
let s:global = self#Feat('global', {})
for s:key in keys(s:global)
  try
    call execute('let g:' . s:key . ' = ' . string(s:global[s:key]))
    call self#log#({'global': s:key, 'value': string(s:global[s:key])})
  catch
    call self#log#({'global': 'failed', 'error': v:exception})
  endtry
endfor
" }}}


" -------
" Options
" -------
"{{{
let g:mapleader = ','

if !isdirectory(g:paths#cache . '/swap_files')
  call mkdir(g:paths#cache . '/swap_files')
endif
if !isdirectory(g:paths#cache . '/undo_files')
  call mkdir(g:paths#cache . '/undo_files')
endif

let &directory = g:paths#cache . '/swap_files'
let &undodir = g:paths#cache . '/undo_files'

set nobackup
set nowritebackup
set swapfile
set undofile

set virtualedit=block
set splitbelow
set splitright
set hidden

set noshowmode
set showcmd
set noruler

set ignorecase
set smartcase
set inccommand=nosplit
set gdefault

set shiftwidth=2
set tabstop=2
set expandtab

set wildignore=*.o
set formatoptions+=j
set listchars=tab:╺─,trail:─,extends:>,precedes:<,nbsp:+ " ,eol:╸ might be cool
set list

set conceallevel=2

set cursorline
set mouse=a
set scrolloff=5

set completeopt-=preview

if executable('rg')
  set grepprg=rg\ --vimgrep
endif

if has('langmap') && exists('+langremap')
  " Prevent that the langmap option applies to characters that result from a
  " mapping.  If set (default), this may break plugins (but it's backward
  " compatible).
  set nolangremap
endif

if has('nvim') || has('gui')
  set guicursor=
        \n:block,
        \v-ve:block-blinkon500-blinkoff500,
        \i:ver25-blinkon500-blinkoff500,
        \r:hor10-blinkon500-blinkoff500
elseif !has('gui_running')
  if $TERM ==# 'xterm-256color'
    let &t_EI = '\<Esc>[2 q'
    let &t_SI = '\<Esc>[5 q'
    let &t_SR = '\<Esc>[3 q'
  endif
endif

set number
augroup RelativeNumbers
  au!

  " Automatically switch between relative numbering and absolute numbering
  au InsertEnter *.* set norelativenumber
  au InsertLeave *.* set relativenumber
augroup END
set relativenumber

augroup VimStartup
  au!

  autocmd BufRead,BufNewFile *.h set filetype=c
  autocmd FileType vim setlocal foldmethod=marker
  " disable scrolloff on the tiny
  autocmd FileType qf  setlocal scrolloff=0

  " When editing a file, always jump to the last known cursor position.
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line('$') && (expand('%:h:t') !=# '.git')|
    \   exe 'normal! g`"' |
    \ endif
augroup END
"}}}

" --------
" Mappings
" --------
"{{{
nnoremap <F1> <nop>
inoremap <F1> <nop>
vnoremap <F1> <nop>
tnoremap <F1> <nop>

nnoremap <esc><esc> :noh<return><esc>
nnoremap <leader>b :ls<return>:b<space>

nnoremap <space>w :w<CR>
nnoremap <space>e :e<space>
nnoremap <space>q :q<CR>
nnoremap <Space>wq :wq<CR>

nnoremap n nzz
nnoremap j gj
nnoremap k gk

nnoremap Y y$
nnoremap H ^
vnoremap H ^
nnoremap L $
vnoremap L $

nnoremap p ]p
nnoremap <leader>p "+]p
nnoremap <leader>P "+P=`]
vnoremap <leader>p "+p=`]

nnoremap <leader>y "+y
onoremap <leader>y "+y
vnoremap <leader>y "+y
nnoremap <leader>yy "+yy
nnoremap <leader>Y "+y$

nnoremap <leader>dd "+dd
vnoremap <leader>d "+d
nnoremap <leader>D "+D

inoremap jj <Esc>
vnoremap < <gv
vnoremap > >gv


if has('nvim')
  tnoremap <Esc><Esc> <c-\><c-n>
endif

nnoremap \| :vsplit %:h<CR>
nnoremap _ :split %:h<CR>
nnoremap <s-tab> gt
"}}}

" --------
" Commands
" --------
"{{{
command! W w
command! Q q
command! E e
command! WQ wq
command! Wq wq
command! WA wa
command! Wa wa
command! QA qa
command! Qa qa
command! Wqa wqa
command! WQa wqa
command! WQA wqa

command! DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
    \ | wincmd p | diffthis

command!
      \ -nargs=1
      \ -complete=expression
      \ Edit try | execute 'e ' . eval(<q-args>) | catch | execute 'e ' . <q-args> | endtry
"}}}


" -------
" Plugins
" -------

call plug#begin(g:paths#plugins)

" -- utilities --

" fat utilities
if self#HasFeat('plugins.vimwiki')
  Plug 'vimwiki/vimwiki'
endif
" The life.
Plug 'justinmk/vim-dirvish'
" Important, for git support
Plug 'lambdalisue/gina.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'prabirshrestha/async.vim'
if executable('go') && executable('make')
  Plug 'RRethy/vim-hexokinase', { 'do': 'make hexokinase' }
endif

" tiny and cool utilities #TpopeFTW
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-scriptease'
Plug 'tpope/vim-rsi'
Plug 'tpope/vim-eunuch'

" -- visual --

Plug 'morhetz/gruvbox'
Plug 'KeitaNakamura/neodark.vim'
Plug 'AlessandroYorba/Alduin'
if self#Feat('plugins.statusline') ==# 'lightline'
  Plug 'itchyny/lightline.vim'
  Plug 'shinchu/lightline-gruvbox.vim'
elseif self#Feat('plugins.statusline') ==# 'crystalline'
  Plug 'rbong/vim-crystalline'
endif
if self#HasFeat('plugins.devicons')
  Plug 'ryanoasis/vim-devicons'
endif

" -- completion --

" sources
Plug 'Shougo/neco-vim', { 'for': 'vim' }
Plug 'Shougo/neoinclude.vim', { 'for': ['cpp', 'c'] }
Plug 'racer-rust/vim-racer', { 'for': 'rust' }
" bonuses
Plug 'Shougo/echodoc.vim'
Plug 'tpope/vim-endwise', { 'for': ['cpp', 'c', 'lua', 'vim', 'ruby', 'elixir'] }

" deoplete
if self#Feat('plugins.completion_engine') ==# 'deoplete'
  Plug 'Shougo/deoplete.nvim'
  Plug 'Shougo/context_filetype.vim'

  " sources
  Plug 'tweekmonster/deoplete-clang2', { 'for': ['cpp', 'c'] }
  Plug 'zchee/deoplete-jedi', { 'for': ['python'] }
  Plug 'Shougo/deoplete-zsh', { 'for': 'zsh' }
  Plug 'carlitux/deoplete-ternjs', { 'for': ['javascript'] }
endif

" nvim-completion-manager
if self#Feat('plugins.completion_engine') ==# 'ncm'
  Plug 'roxma/nvim-completion-manager'

  " sources
  Plug 'roxma/ncm-clang', { 'for': [ 'cpp', 'c']}
  Plug 'roxma/nvim-cm-racer', { 'for': 'rust' }

  let g:lc_plug_config = {}
  if self#system#is_darwin()
    let g:lc_plug_config.tag = 'binary-*-x86_64-apple-darwin'
  elseif self#system#is_linux()
    let g:lc_plug_config.tag = 'binary-*-x86_64-unknown-linux-musl'
  elseif self#system#is_win()
    let g:lc_plug_config.tag = 'binary-*-x86_64-pc-windows-gnu'
  else
    let g:lc_plug_config.branch = 'next'
    let g:lc_plug_config.do = 'make release'
  endif
  Plug 'autozimu/LanguageClient-neovim', g:lc_plug_config

endif

" asyncomplete
if self#Feat('plugins.completion_engine') ==# 'asyncomplete'
  Plug 'prabirshrestha/asyncomplete.vim'
  Plug 'prabirshrestha/vim-lsp'
  Plug 'prabirshrestha/asyncomplete-lsp.vim'
  Plug 'prabirshrestha/asyncomplete-file.vim'
  Plug 'prabirshrestha/asyncomplete-buffer.vim'
  Plug 'prabirshrestha/asyncomplete-necovim.vim'
  Plug 'prabirshrestha/asyncomplete-tags.vim'
  Plug 'yami-beta/asyncomplete-omni.vim'
endif

" -- editing facilities

" TODO: remove it
Plug 'ervandew/supertab'
Plug 'jiangmiao/auto-pairs'
Plug 'Yggdroot/indentLine'

" -- linting and formating --

if self#HasFeat('plugins.ale')
  Plug 'w0rp/ale'
endif
Plug 'sbdchd/neoformat'

" -- syntax --

" the mother of all repos
Plug 'sheerun/vim-polyglot'

" programming languages
Plug 'othree/yajs.vim'
Plug 'justinmk/vim-syntax-extra'
Plug 'lifepillar/pgsql.vim'
Plug 'vmchale/ion-vim'
Plug 'slashmili/alchemist.vim'


" additions through features.toml
call map(self#Feat('plugins.additions', []), {_i, val -> plug#(val)})

call plug#end()

filetype plugin indent on
syntax enable


"End Plugins -------------------------

" ---------------------
" -- Plugins options --
" ---------------------

call self#WithFeat('paths.python2', { path -> vext#Let('python_host_prog', path)})
call self#WithFeat('paths.python3', { path -> vext#Let('python3_host_prog', path)})

" -- Some simple plugin configs --
let g:SuperTabDefaultCompletionType = '<c-n>'
let g:indentLine_char = '│'
let g:dirvish_relative_paths = 1
let g:Hexokinase_highlighters = ['background']

" -- colorscheme --
set background=dark
if $COLORTERM ==# 'truecolor' && has('termguicolors')
  set termguicolors
endif
let s:colorscheme = self#Feat('general.colorscheme', 'gruvbox')
if s:colorscheme ==# 'neodark'
  let g:neodark#background = '#202020'
  colorscheme neodark
elseif s:colorscheme ==# 'alduin'
  colorscheme alduin
elseif s:colorscheme ==# 'plastic'
  colorscheme plastic
else
  let g:gruvbox_italic = 1
  let g:gruvbox_contrast_dark = 'medium'
  colorscheme gruvbox
endif

" grubox colorscheme for terminal
if &termguicolors
  let g:terminal_color_0 = '#282828'
  let g:terminal_color_1 = '#cc241d'
  let g:terminal_color_2 = '#98971a'
  let g:terminal_color_3 = '#d79921'
  let g:terminal_color_4 = '#458588'
  let g:terminal_color_5 = '#b16286'
  let g:terminal_color_6 = '#689d6a'
  let g:terminal_color_7 = '#a89984'
  let g:terminal_color_8 = '#928374'
  let g:terminal_color_9 = '#fb4934'
  let g:terminal_color_10 ='#b8bb26'
  let g:terminal_color_11 ='#fabd2f'
  let g:terminal_color_12 ='#83a598'
  let g:terminal_color_13 ='#d3869b'
  let g:terminal_color_14 ='#8ec07c'
  let g:terminal_color_15 ='#ebdbb2'
endif

" -- custom plugins --
nnoremap <c-c><c-c> <Plug>SlimeSend
vnoremap <c-c> <Plug>SlimeSend

" -- syntax --
" cpp
let g:cpp_class_scope_highlight = 1
" sql
let g:sql_type_default = 'pgsql'
" html
let g:html_indent_autotags = 'html,body' " do not indent direct children of these tags
" markdown
let g:markdown_fenced_languages = ['rust', 'html', 'python', 'lua', 'vim', 'c', 'javascript']
" -- vimwiki --
let g:vimwiki_dir_link = 'index'
let g:vimwiki_hl_headers = 1
let g:vimwiki_table_mappings = 0
let s:cours_wiki = {
      \ 'path': '~/Documents/cours/wiki',
      \ 'path_html': '~/Documents/cours/html',
      \ 'template_path': '~/Documents/cours/templates',
      \ 'template_default': 'default.html',
      \ 'template_ext': '.tpl',
      \ 'auto_export': 1
      \}
let g:vimwiki_list = [{}, s:cours_wiki]

" -- completions engine --
let g:racer_cmd = expand('~') . '/.cargo/bin/racer'
let s:rustc_sysroot = system('rustc --print sysroot')
let $RUST_SRC_PATH = strcharpart(s:rustc_sysroot, 0, strlen(s:rustc_sysroot) - 1) . '/lib/rustlib/src/rust/src'
let g:racer_experimental_completer = 1
let g:echodoc_enable_at_startup = 1

" - deoplete (and other completion stuff) -
if self#Feat('plugins.completion_engine') ==# 'deoplete'
  call config#deoplete#load()
endif

" - asyncomplete -
if self#Feat('plugins.completion_engine') ==# 'asyncomplete'
  call config#asyncomplete#load()
  call config#lsp#load()
endif

" - nvim_completion_manager -
if self#Feat('plugins.completion_engine') ==# 'ncm'
  runtime! config/nvim-cm.vim
endif

if self#Feat('plugins.completion_engine') ==# 'ale'
  call config#ale_completion#load()
endif


" -- statusline --
let s:StatuslineLoader = function('config#' . self#Feat('plugins.statusline', 'statusline') . '#load')
call s:StatuslineLoader()

" -- ale --
if self#HasFeat('plugins.ale')
  call config#ale#load()
endif
