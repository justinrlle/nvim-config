if exists('g:loaded_slime') || !has('nvim')
  finish
endif
let g:loaded_slime = 1

let s:trim = { str -> substitute(str, '^\s*\(.\{-}\)\s*$', '\1', '') }

function! s:SplitRepl(args) abort
  let l:args = split(a:args, ' ')
  if len(l:args) == 0
    throw 'Err: please provide a repl name'
  endif
  if len(l:args) == 1
    call self#slime#StartRepl(l:args[0], l:args[0], &filetype)
  elseif len(l:args) == 2
      call self#slime#StartRepl(l:args[0], l:args[1], &filetype)
  else
    call self#slime#StartRepl(l:args[0], l:args[1], l:args[2])
  endif
endfunction

function! s:StopWrapper(name) abort
  if len(a:name) == 0
    call self#slime#StopRepl()
  else
    call self#slime#StopRepl(a:name)
  endif
endfunction

function! s:VisualSendWrapper() abort
  call self#slime#SendData(vext#GetVisual())
endfunction

command! -nargs=* Slime :call s:SplitRepl(<q-args>)
command! -nargs=? SlimeStop :call s:StopWrapper(<q-args>)
command! SlimeSend :call self#slime#SendTopLevelDef()
command! -range SlimeSendV :call s:VisualSendWrapper()

nnoremap <Plug>SlimeSend :SlimeSend<CR>

vnoremap <Plug>SlimeSend :SlimeSendV<CR>
