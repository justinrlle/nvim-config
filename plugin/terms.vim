if exists('g:loaded_terms') || !has('nvim')
  finish
endif
let g:loaded_terms = 1

let s:trim = { str -> substitute(str, '^\s*\(.\{-}\)\s*$', '\1', '') }
function! s:SplitRun(cmd, count, mods) abort
  call self#terms#Split({ -> self#terms#Run(a:cmd) }, a:count, a:mods)
endfunction

function! s:SplitBound(cmd, args, count, mods) abort
  call self#terms#Split({ -> self#terms#Bind(a:cmd, a:args) }, a:count, a:mods)
endfunction

function! s:SplitBind(cmd, count, mods) abort
  let l:idx = stridx(a:cmd, ' ')
  let l:base_cmd = strpart(a:cmd, 0, l:idx)
  let l:args = trim(strpart(a:cmd, l:idx))
  call self#terms#Split({ -> self#terms#Bind(l:base_cmd, l:args) }, a:count, a:mods)
endfunction

command! -nargs=* -complete=shellcmd -count
      \ TSplit call s:SplitRun(<q-args>, <count>, <q-mods>)
command! -nargs=* -complete=shellcmd
      \ TRun call self#terms#Run(<q-args>)
command! -nargs=* -complete=shellcmd -count
      \ TSplitBind call s:SplitBind(<q-args>, <count>, <q-mods>)
command! -nargs=* -count
      \ Howdoi call s:SplitBound('howdoi', <q-args>, <count>, <q-mods>)
