#!/bin/sh

# AUTHOR: justinrlle <justin.rerolle@protonmail.com>
# Checks that vim-plug is correctly installed (will install it if that's not the case)
# Works only on unix systems.

cd "$(dirname "$0")" || exit

check_vim_plug() {
  nvim -u ./test-vim-plug.vim > /dev/null 2>&1
}

show() {
  printf "$( tput setaf "$1" )%s$( tput sgr0 ): %s\\n" "$2" "$3"
}

info() {
  show 3 INFO "$*"
}

error() {
  show 1 ERR "$*"
}

success() {
  show 2 SUCCESS "$*"
}

if check_vim_plug; then
  success 'Vim-Plug is already installed!'
else
  info 'No Vim-Plug installed, installing...'
  curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  if check_vim_plug; then
    success 'Vim-plug has been installed!'
  else
    error 'Failed to install Vim-Plug, please checks the logs'
  fi
fi
