<#
  AUTHOR: justinrlle <justin.rerolle@protonmail.com>
  Checks that vim-plug is correctly installed (will install it if that's not the case)
  Works only on windows.
#>

$installPath = "$env:USERPROFILE\AppData\Local\nvim\site\autoload"
$uri = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

$vimplug_test = Join-Path (Split-Path $MyInvocation.MyCommand.Path) "test-vim-plug.vim"

function Check-Vim-Plug {
    nvim -u "$vimplug_test" 2>&1 | out-null
    if ($LastExitCode -eq 0) {
        return $True
    } else {
        return $False
    }
}

function Show([string]$title, [string]$color, [string]$message) {
    Write-Host "$title" -ForegroundColor "$color" -NoNewLine
    Write-Host ": $message"
}

function Info([string]$message) {
    Show -title "INFO" -color "Yellow" "$message"
}

function Error([string]$message) {
    Show -title "ERROR" -color "Red" "$message"
}

function Success([string]$message) {
    Show -title "SUCCESS" -color "Green" "$message"
}

if (Check-Vim-Plug) {
    Success 'Vim-Plug is already installed!'
} else {
    Info 'No Vim-Plug installed, installing...'
    if (-Not (Test-Path -PathType Container "$installPath")) {
        New-Item -ItemType Directory -Path "$installPath"
    }
    (New-Object Net.WebClient).DownloadFile($uri, "$installPath\plug.vim")
    if (Check-Vim-Plug) {
        Success 'Vim-plug has been installed!'
    } else {
        Error 'Failed to install Vim-Plug, please checks the logs'
    }
}
