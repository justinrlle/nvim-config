" checks if vim-plug is available in autoload path
try
  call plug#please_dont_exist()
  " If a function is undefined, the error E117 is raised.
catch /^Vim\%((\a\+)\)\=:E117/
  if exists('*plug#')
    echo 'Vim-Plug already present on this system!'
    quit
  else
    echo 'Vim-Plug is not present: please install it'
    cquit
  endif
endtry
