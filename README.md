My neovim config
================

The goal of this config is to be easily portable. You clone it, you set up a toml file, and you run
`nvim -u path/to/init.vim` and you'll get a neovim with it's own configuration.

You should be able to setup where all of this is installed, where the temporary files are stocked,
what major plugins you'll want...

> Let's be clear: the purpose of this config is not to be some kind of [Spacevim] or [spf13]
> distribution. This is my config, but it's done in a way that I can adapt it to any setup I
> might have.

It relies heavily on [dein]. Currently the pass to the install of dein is the `dein/` folder, in
which you can find the installer for `dein`. Of courses this repo is my personal config, so you
shouldn't blindly use it, but you can always have a look, maybe discover something about vim, or
just see an anti pattern and then correct me... Anyway, just do what you want with it.

How to install
---

You need `git` installed. Then just git clone this repo:

```bash
git clone https://gitlab.com/justinrlle/nvim-config.git nvim/config/path
# nvim/config/path is ~/.config/nvim on unix, ~/Appdata/Roaming/nvim on windows
cd nvim/config/path
sh dein/installer.sh dein # will install dein inside the dein folder
nvim # will automatically install all plugins
```

Right now you can't really configure where dein will be installed, but that's planned.


To do
---

- [ ] folder local settings with a toml file
  - [x] get code from dein (with the license, and the motion of dein)
  - [x] write some spec (more on this later)
  - [x] set it up, and use it inside my config
  - [ ] document it
- [ ] support correctly windows
  - [ ] solve mysterious bug with `dein#install()`
  - [ ] check that all the rest of the setup does nothing wrong
  - [ ] add config for [neovim-qt]
- [ ] use features to allow a more customizable installation
  - [ ] dein install path
  - [ ] undo/swap files path

About the _features_ toml file
------------------------------

### The base idea

A file in the toml format that would allow global configuration (in the config folder), and a file
per folder that would overrite some config.

### The motivation

Some times, I've found `ftplugin` to be not enough. I want _per project_ configuration, a bit like
with editor config, but with support for vim features (for example, some include paths specific to
a project). And in those cases, `modeline` is not enough too (I don't want to write a modeline in
each files). In fact, this could lead to a full vimscript implementation of editor-config (python
is currently required to just set some settings in vim...).

This should also allow to have more _granularity_ in the configuration of neovim, and would allow
to customize the install without it being tracked by the VCS.

### The api

I see mainly two functions:
```vim
let var = self#features#Get('target')
if self#features#Has('target')
  ...
endif
```
The two supports paths, eg `self#features#Get('some.long.target')`, and it will go down the path.

### The questions

There is some basic questions such as _What will be the name of the file?_, but those are not
really important. The real questions are about the format: should it just be things parsed down and
that you can query, using `self#features#Get('plugins.ale')`? Or should I enable some kind of
smart parsing, with a section called `options`, and set up the matching options? As of today, I got
something like this in mind:

```toml
[global]
ale_cpp_flags = '-Wall -Wextra -std=c++14 -pedantic' # will set g:ale_cpp_flags to this value

[options] 
path = { mode = 'add', value = 'mylib/include' } # will be executed as set path+=mylib/include

# all other values will just be stored
[plugins]
ale = true

[plugins.deoplete]
clang_paths = { lib = '/usr/lib/libclang.so', header = '/usr/lib/clang' }
```

Of course, to stop from malicious usage (like for modeline), the options and variables allowed to
be setup this way are limited. And by limited, I mean that you explicitly set them with a variable
along the lines of `g:feat_options_allowed`.

I like the idea of the __options__ section, but I don't really see any interest in the __global__
section, apart from less code to write, but it would increase it in the same time:

```vim
" with support for the global section
let g:feat_globals_allowed = [
      \ 'ale_cpp_flags',
      \ 'ale_c_flgas',
      \ 'sql_type_default',
      \ 'SuperTabDefaultCompletionType',
      \ ... ]


" without the support
let g:SuperTabDefaultCompletionType = self#features#Get('super_tab')
let g:sql_type_default = self#features#Get('sql.default_type')
let g:ale_cpp_flags = self#features#Get('cpp.flags') " or self#features#Get('flags.cpp')
let g:ale_c_flags = self#features#Get('c.flags') " or self#features#Get('flags.c')
```

The first approach forces the toml file to be quite verbose, while the second focus the verbosness
in the init file. The first one also forces you to defines this before loading the `features.toml`
files. To be honest, I prefer the second. 

### What I've done till now

I've settled with the `options` section feature, and forgot about the `globals` section. The file
can be loaded with a toml parser taken from [dein], the `options` section will be correctly
interpreted, and the others values will be stored. The current API is near good, all I miss is the
auto loading of the different files (a global `features.toml`, and the local `features.toml`).

> TODO: update this README


[dein]: https://github.com/Shougo/dein.vim
[neovim-qt]: https://github.com/equalsraf/neovim-qt
[Spacevim]: https://www.google.de/search?q=spacevim
[spf13]: http://vim.spf13.com/
