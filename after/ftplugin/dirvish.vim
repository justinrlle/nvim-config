nnoremap <buffer> <S-CR> :split<CR>:call dirvish#open("edit", 0)<CR>

command! -buffer -nargs=1 NewFile :call writefile([], expand('%') . <q-args>, 'a')
command! -buffer -nargs=1 NewDir :call mkdir(expand('%') . <q-args>)
