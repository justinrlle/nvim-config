"=============================================================================
" FILE: vimwiki.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================
scriptencoding utf8

"
" personal settings for vimwiki
" currently, it's just some spell options, and a workaround to insert some
" math symbols
"

setlocal spell
setlocal spelllang=fr,en_us

nnoremap <buffer> - :VimwikiGoBackLink<CR>

let s:prefix = get(g:, 'vimwiki_symbols_prefix', '<c-s>')

let s:mappings = [
      \   [ 'pt', '∀' ],
      \   [ 'ap', '∈' ],
      \ ]

for s:map in s:mappings
  execute 'inoremap <buffer> ' . s:prefix . s:map[0] . ' ' . s:map[1]
endfor

command! VimwikiHelpSymbols :call VimwikiHelpSymbols()

function! VimwikiHelpSymbols() abort
  echo 'prefix: ' . s:prefix
  for l:map in s:mappings
    echo '  <prefix>' . l:map[0] . ' => ' . l:map[1]
  endfor
endfunction
