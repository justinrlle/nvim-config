"=============================================================================
" FILE: vext.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

"
" Some extensions of the vim script defaults functions with some utilities
" I've came up with.
"

" Code from bairui@#vim.freenode
" https://gist.github.com/3322468
function! vext#Flatten(list) abort
  let l:val = []
  for l:elem in a:list
    if type(l:elem) == v:t_list
      call extend(l:val, vext#Flatten(l:elem))
    else
      call add(l:val, l:elem)
    endif
    unlet l:elem
  endfor
  return l:val
endfunction

" Function from xolox
" Code found on [stackoverflow](https://stackoverflow.com/a/6271254)
function! vext#GetVisual() abort
  let [l:line_start, l:column_start] = getpos("'<")[1:2]
  let [l:line_end, l:column_end] = getpos("'>")[1:2]
  let l:lines = getline(l:line_start, l:line_end)
  if len(l:lines) == 0
    return ''
  endif
  let l:lines[-1] = l:lines[-1][: l:column_end - (&selection ==# 'inclusive' ? 1 : 2)]
  let l:lines[0] = l:lines[0][l:column_start - 1:]
  return join(l:lines, "\n")
endfunction

function! vext#Extend(dic1, dic2, ...) abort
  for l:key in keys(a:dic2)
    if has_key(a:dic1, l:key)
      let l:type = type(a:dic1[l:key])
      if l:type != type(a:dic2[l:key])
        let a:dic1[l:key] = a:dic2[l:key]
      else
        if l:type == v:t_dict
          call vext#Extend(a:dic1[l:key], a:dic2[l:key])
        elseif l:type == v:t_list
          call extend(a:dic1[l:key], a:dic2[l:key])
        else
          let a:dic1[l:key] = a:dic2[l:key]
        endif
      endif
    else
      let a:dic1[l:key] = a:dic2[l:key]
    endif
  endfor

  return a:dic1
endfunction

function! vext#Let(name, value) abort
  execute 'let g:' . a:name . ' = ' . string(a:value)
endfunction
