"=============================================================================
" FILE: self.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

"
" Contains some basic info and some functions
" 'self' is used as a prefix to avoid any conflict with plugins
"


" A wrapper around self#features#Feature with a default value, the api
" ressembles the standard vim function get(), with a call to expand().
function! self#Feat(featname, ...) abort
  let l:default = a:0 >= 1 ? a:1 : v:null
  let l:value = self#features#Feature(a:featname)
  if type(l:value) == v:t_string
    return expand(l:value)
  endif
  return type(l:value) == type(v:null) ? l:default : l:value
endfunction

" Proxy to self#features#Has, shorter name.
function! self#HasFeat(featname) abort
  return self#features#Has(a:featname)
endfunction

function! self#WithFeat(featname, do) abort
  let l:feat = self#Feat(a:featname)
  if type(l:feat) != type(v:null)
    return a:do(l:feat)
  endif
  return v:null
endfunction

function! self#clean_dein_plugins() abort
  if exists('*dein#check_clean')
    let l:to_clean = dein#check_clean()
    if empty(l:to_clean)
      call dein#util#_notify('no pluggins to remove')
    else
      call dein#util#_notify('removing disabled plugins...')
      for  l:plugin in l:to_clean
        call dein#job#start('rm -rf ' . l:plugin)
      endfor
    endif
  else
    echom 'dein is not installed'
  endif
endfunction
