"=============================================================================
" FILE: toml.vim
" AUTHOR:  Shougo Matsushita <Shougo.Matsu at gmail.com>
" License: MIT license
"=============================================================================

"
" This file is the code used in the plugin dein.vim by Shougo
" <https://github.com/Shougo/dein.vim>
" The only modifications are on the autoload paths (eg dein#toml => self#toml)
" And on the variables scopes (eg let input => let l:input)
"

"
" Public api
"
function! self#toml#parse(text) abort
  let l:input = {
  \   'text': a:text,
  \   'p': 0,
  \   'length': strlen(a:text),
  \}
  return s:_parse(l:input)
endfunction

function! self#toml#parse_file(filename) abort
  if !filereadable(a:filename)
    throw printf("vital: Text.TOML: No such file `%s'.", a:filename)
  endif

  let l:text = join(readfile(a:filename), "\n")
  " fileencoding is always utf8
  return self#toml#parse(iconv(l:text, 'utf8', &encoding))
endfunction

"
" private api
"
" work around: '[^\r\n]*' doesn't work well in old-vim, but "[^\r\n]*" works well
let s:skip_pattern = '\C^\%(\_s\+\|' . "#[^\r\n]*" . '\)'
let s:table_name_pattern = '\%([^ [:tab:]#.[\]=]\+\)'
let s:table_key_pattern = s:table_name_pattern

function! s:_skip(input) abort
  while s:_match(a:input, '\%(\_s\|#\)')
    let a:input.p = matchend(a:input.text, s:skip_pattern, a:input.p)
  endwhile
endfunction

" XXX: old engine is faster than NFA engine (in this context).
if exists('+regexpengine')
  let s:regex_prefix = '\%#=1\C^'
else
  let s:regex_prefix = '\C^'
endif

function! s:_consume(input, pattern) abort
  call s:_skip(a:input)
  let l:end = matchend(a:input.text, s:regex_prefix . a:pattern, a:input.p)

  if l:end == -1
    call s:_error(a:input)
  elseif l:end == a:input.p
    return ''
  endif

  let l:matched = strpart(a:input.text, a:input.p, l:end - a:input.p)
  let a:input.p = l:end
  return l:matched
endfunction

function! s:_match(input, pattern) abort
  return match(a:input.text, s:regex_prefix . a:pattern, a:input.p) != -1
endfunction

function! s:_eof(input) abort
  return a:input.p >= a:input.length
endfunction

function! s:_error(input) abort
  let l:buf = []
  let l:offset = 0
  while (a:input.p + l:offset) < a:input.length && a:input.text[a:input.p + l:offset] !~# "[\r\n]"
    let l:buf += [a:input.text[a:input.p + l:offset]]
    let l:offset += 1
  endwhile

  throw printf("vital: Text.TOML: Illegal toml format at L%d:`%s':%d.",
      \ len(split(a:input.text[: a:input.p], "\n", 1)),
      \ join(l:buf, ''), a:input.p)
endfunction

function! s:_parse(input) abort
  let l:data = {}

  call s:_skip(a:input)
  while !s:_eof(a:input)
    if s:_match(a:input, '[^ [:tab:]#.[\]]')
      let l:key = s:_key(a:input)
      call s:_equals(a:input)
      let l:value = s:_value(a:input)

      call s:_put_dict(l:data, l:key, l:value)

      unlet l:value
    elseif s:_match(a:input, '\[\[')
      let [l:key, l:value] = s:_array_of_tables(a:input)

      call s:_put_array(l:data, l:key, l:value)

      unlet l:value
    elseif s:_match(a:input, '\[')
      let [l:key, l:value] = s:_table(a:input)

      call s:_put_dict(l:data, l:key, l:value)

      unlet l:value
    else
      call s:_error(a:input)
    endif
    call s:_skip(a:input)
  endwhile

  return l:data
endfunction

function! s:_key(input) abort
  let l:s = s:_consume(a:input, s:table_key_pattern)
  return l:s
endfunction

function! s:_equals(input) abort
  call s:_consume(a:input, '=')
  return '='
endfunction

function! s:_value(input) abort
  call s:_skip(a:input)

  if s:_match(a:input, '"\{3}')
    return s:_multiline_basic_string(a:input)
  elseif s:_match(a:input, '"\{1}')
    return s:_basic_string(a:input)
  elseif s:_match(a:input, "'\\{3}")
    return s:_multiline_literal(a:input)
  elseif s:_match(a:input, "'\\{1}")
    return s:_literal(a:input)
  elseif s:_match(a:input, '\[')
    return s:_array(a:input)
  elseif s:_match(a:input, '\%(true\|false\)')
    return s:_boolean(a:input)
  elseif s:_match(a:input, '\d\{4}-')
    return s:_datetime(a:input)
  elseif s:_match(a:input, '[+-]\?\%(\d\+\.\d\|\d\+\%(\.\d\+\)\?[eE]\)')
    return s:_float(a:input)
  elseif s:_match(a:input, '{')
    return s:_inline_table(a:input)
  else
    return s:_integer(a:input)
  endif
endfunction

"
" String
"
function! s:_basic_string(input) abort
  let l:s = s:_consume(a:input, '"\%(\\"\|[^"]\)*"')
  let l:s = l:s[1 : -2]
  return s:_unescape(l:s)
endfunction

function! s:_multiline_basic_string(input) abort
  let l:s = s:_consume(a:input, '"\{3}\_.\{-}"\{3}')
  let l:s = l:s[3 : -4]
  let l:s = substitute(l:s, "^\n", '', '')
  let l:s = substitute(l:s, '\\' . "\n" . '\_s*', '', 'g')
  return s:_unescape(l:s)
endfunction

function! s:_literal(input) abort
  let l:s = s:_consume(a:input, "'[^']*'")
  return l:s[1 : -2]
endfunction

function! s:_multiline_literal(input) abort
  let l:s = s:_consume(a:input, "'\\{3}.\\{-}'\\{3}")
  let l:s = l:s[3 : -4]
  let l:s = substitute(l:s, "^\n", '', '')
  return l:s
endfunction

"
" Integer
"
function! s:_integer(input) abort
  let l:s = s:_consume(a:input, '[+-]\?\d\+')
  return str2nr(l:s)
endfunction

"
" Float
"
function! s:_float(input) abort
  if s:_match(a:input, '[+-]\?[0-9.]\+[eE][+-]\?\d\+')
    return s:_exponent(a:input)
  else
    return s:_fractional(a:input)
  endif
endfunction

function! s:_fractional(input) abort
  let l:s = s:_consume(a:input, '[+-]\?[0-9.]\+')
  return str2float(l:s)
endfunction

function! s:_exponent(input) abort
  let l:s = s:_consume(a:input, '[+-]\?[0-9.]\+[eE][+-]\?\d\+')
  return str2float(l:s)
endfunction

"
" Boolean
"
function! s:_boolean(input) abort
  let l:s = s:_consume(a:input, '\%(true\|false\)')
  return (l:s ==# 'true') ? v:true : v:false
endfunction

"
" Datetime
"
function! s:_datetime(input) abort
  let l:s = s:_consume(a:input, '\d\{4}-\d\{2}-\d\{2}T\d\{2}:\d\{2}:\d\{2}\%(Z\|-\?\d\{2}:\d\{2}\|\.\d\+-\d\{2}:\d\{2}\)')
  return l:s
endfunction

"
" Array
"
function! s:_array(input) abort
  let l:ary = []
  let l:_ = s:_consume(a:input, '\[')
  call s:_skip(a:input)
  while !s:_eof(a:input) && !s:_match(a:input, '\]')
    let l:ary += [s:_value(a:input)]
    call s:_consume(a:input, ',\?')
    call s:_skip(a:input)
  endwhile
  let l:_ = s:_consume(a:input, '\]')
  return l:ary
endfunction

"
" Table
"
function! s:_table(input) abort
  let l:tbl = {}
  let l:name = s:_consume(a:input, '\[\s*' . s:table_name_pattern . '\%(\s*\.\s*' . s:table_name_pattern . '\)*\s*\]')
  let l:name = l:name[1 : -2]
  call s:_skip(a:input)
  " while !s:_eof(a:input) && !s:_match(a:input, '\[\{1,2}[a-zA-Z0-9.]\+\]\{1,2}')
  while !s:_eof(a:input) && !s:_match(a:input, '\[')
    let l:key = s:_key(a:input)
    call s:_equals(a:input)
    let l:value = s:_value(a:input)

    let l:tbl[l:key] = l:value

    unlet l:value
    call s:_skip(a:input)
  endwhile
  return [l:name, l:tbl]
endfunction

"
" Inline Table
"
function! s:_inline_table(input) abort
  let l:tbl = {}
  let l:_ = s:_consume(a:input, '{')
  call s:_skip(a:input)
  while !s:_eof(a:input) && !s:_match(a:input, '}')
    let l:key = s:_key(a:input)
    call s:_equals(a:input)
    let l:tbl[l:key] = s:_value(a:input)
    call s:_consume(a:input, ',\?')
    call s:_skip(a:input)
  endwhile
  let l:_ = s:_consume(a:input, '}')
  return l:tbl
endfunction

"
" Array of tables
"
function! s:_array_of_tables(input) abort
  let l:tbl = {}
  let l:name = s:_consume(a:input, '\[\[\s*' . s:table_name_pattern . '\%(\s*\.\s*' . s:table_name_pattern . '\)*\s*\]\]')
  let l:name = l:name[2 : -3]
  call s:_skip(a:input)
  " while !s:_eof(a:input) && !s:_match(a:input, '\[\{1,2}[a-zA-Z0-9.]\+\]\{1,2}')
  while !s:_eof(a:input) && !s:_match(a:input, '\[')
    let l:key = s:_key(a:input)
    call s:_equals(a:input)
    let l:value = s:_value(a:input)

    let l:tbl[l:key] = l:value

    unlet l:value
    call s:_skip(a:input)
  endwhile
  return [l:name, [l:tbl]]
endfunction

function! s:_unescape(text) abort
  let l:text = a:text
  let l:text = substitute(l:text, '\\"', '"', 'g')
  let l:text = substitute(l:text, '\\b', "\b", 'g')
  let l:text = substitute(l:text, '\\t', "\t", 'g')
  let l:text = substitute(l:text, '\\n', "\n", 'g')
  let l:text = substitute(l:text, '\\f', "\f", 'g')
  let l:text = substitute(l:text, '\\r', "\r", 'g')
  let l:text = substitute(l:text, '\\/', '/', 'g')
  let l:text = substitute(l:text, '\\\\', '\', 'g')
  let l:text = substitute(l:text, '\C\\u\(\x\{4}\)', '\=s:_nr2char("0x" . submatch(1))', 'g')
  let l:text = substitute(l:text, '\C\\U\(\x\{8}\)', '\=s:_nr2char("0x" . submatch(1))', 'g')
  return l:text
endfunction

function! s:_nr2char(nr) abort
  return iconv(nr2char(a:nr), &encoding, 'utf8')
endfunction

function! s:_put_dict(dict, key, value) abort
  let l:keys = split(a:key, '\.')

  let l:ref = a:dict
  for l:key in l:keys[ : -2]
    if has_key(l:ref, l:key) && type(l:ref[l:key]) == type({})
      let l:ref = l:ref[l:key]
    elseif has_key(l:ref, l:key) && type(l:ref[l:key]) == type([])
      let l:ref = l:ref[l:key][-1]
    else
      let l:ref[l:key] = {}
      let l:ref = l:ref[l:key]
    endif
  endfor

  let l:ref[l:keys[-1]] = a:value
endfunction

function! s:_put_array(dict, key, value) abort
  let l:keys = split(a:key, '\.')

  let l:ref = a:dict
  for l:key in l:keys[ : -2]
    let l:ref[l:key] = get(l:ref, l:key, {})

    if type(l:ref[l:key]) == type([])
      let l:ref = l:ref[l:key][-1]
    else
      let l:ref = l:ref[l:key]
    endif
  endfor

  let l:ref[l:keys[-1]] = get(l:ref, l:keys[-1], []) + a:value
endfunction

" vim:set et ts=2 sts=2 sw=2 tw=0:
