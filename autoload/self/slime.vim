"=============================================================================
" FILE: slime.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

let s:jobs = {}
let g:self#slime#default_job = ''

function! s:CreateJob(cmd, opts) abort
  let l:job = deepcopy(a:opts)
  function! l:job.on_exit(_id, code, _type) dict abort
    let self.exit_code = a:code
  endfunction

  let l:job.winid = win_getid()
  let l:job.jobid = termopen(a:cmd, job)
  let s:jobs[l:job.name] = l:job
  let b:job = l:job
endfunction

function! self#slime#StartRepl(name, cmd, ft) abort
  let l:winid = win_getid()
  call self#terms#Split({-> s:CreateJob(a:cmd, {'name': a:name})}, 0, '')
  let g:self#slime#default_job = a:name
  let &filetype = a:ft
  call win_gotoid(l:winid)
endfunction

function! self#slime#GetJobs() abort
  return s:jobs
endfunction

function! self#slime#GetDefaultJob() abort
  let l:default_job = get(g:, 'slime_default_job',
                      \ get(b:, 'slime_default_job',
                      \ get(g:, 'self#slime#default_job', '')))
  if len(l:default_job) == 0
    throw 'Slime(2): please set a default slime job'
  endif
  return l:default_job
endfunction


function! self#slime#StopRepl(...) abort
  if a:0 == 0
    let l:jobname = self#slime#GetDefaultJob()
  else
    let l:jobname = a:1
  endif
  if !has_key(s:jobs, l:jobname)
    throw 'Slime(1): Job ' . string(l:jobname) . ' is not a known job'
  endif
  let l:job = s:jobs[l:jobname]
  call jobstop(l:job.jobid)
  let g:self#slime#default_job = ''
  unlet s:jobs[l:jobname]
  return l:job
endfunction

function! self#slime#SendData(data, ...) abort
  if a:0 == 0
    let l:jobname = self#slime#GetDefaultJob()
  else
    let l:jobname = a:1
  endif
  let l:job = s:jobs[l:jobname]
  call jobsend(l:job.jobid, a:data)
endfunction

function! self#slime#SendTopLevelDef() abort
  let l:TopLevelDef = function('self#slime#' . &filetype . '#TopLevelDef')
  call self#slime#SendData(l:TopLevelDef() . "\n")
endfunction
