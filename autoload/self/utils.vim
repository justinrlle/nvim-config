"=============================================================================
" FILE: toml.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

"
" Some tiny utils I've came up with
"

let s:linux_default_opener = 'xdg-open'

function! self#utils#cross_open(target) abort
  let l:opener = ''
  if self#system#is_system('darwin')
    let l:opener = 'open'
  endif
  if self#system#is_system('linux')
    let l:linux_opener = get(g:, 'linux_url_opener', s:linux_default_opener)
    if executable(l:linux_opener)
      let l:opener = l:linux_opener
    elseif executable(s:linux_default_opener)
      let l:opener = s:linux_default_opener
    endif
  endif
  if self#system#is_system('win')
    let l:opener = 'explorer'
  endif
  return system(l:opener . ' ' . shellescape(a:target))
endfunction

function! self#utils#open_repo() abort
  let l:o_reg = @o
  normal! "oyi'
  let l:repo = @o
  let @o = l:o_reg
  let g:repo = l:repo
  " full url
  if stridx(l:repo, 'https://') == 0
    return self#utils#cross_open(l:repo)
  endif
  " TODO: check if a path
  " repo name
  if stridx(l:repo, '/') > 0
    return self#utils#cross_open('https://github.com/' . l:repo)
  endif
  " vim.org script
  return self#utils#cross_open('https://github.com/vim-scripts' . l:repo)
endfunction

function! self#utils#column_delimiter() abort
if empty(&colorcolumn)
  let &colorcolumn = join(range(81,256),',')
else
  let &colorcolumn = ''
endif
endfunction

function! self#utils#hard_mode() abort
  nnoremap <left> <nop>
  nnoremap <right> <nop>
  nnoremap <up> <nop>
  nnoremap <down> <nop>
  inoremap <left> <nop>
  inoremap <right> <nop>
  inoremap <up> <nop>
  inoremap <down> <nop>
endfunction
function! self#utils#easy_mode() abort
  nnoremap <left> <left>
  nnoremap <right> <right>
  nnoremap <up> <up>
  nnoremap <down> <down>
  inoremap <left> <left>
  inoremap <right> <right>
  inoremap <up> <up>
  inoremap <down> <down>
endfunction

