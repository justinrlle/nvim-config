"=============================================================================
" FILE: features.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================


let s:features = {}
let s:new_options = {}
let s:options = {}
let s:loaded_files = []

function! self#features#_Load(filename) abort
  call add(s:loaded_files, a:filename)
  " parse the toml file
  let l:config = {}
  try
    let l:config = self#toml#parse_file(a:filename)
  catch /^vital: Text.TOML: No such file/
    throw 'features: ' . a:filename . ' file not found'
  endtry
  " if toml file has an 'options' section, extract it and add it to thoses
  " currently loaded.
  if index(keys(l:config), 'options') >= 0
    let l:options = l:config['options']
    unlet l:config['options']
    call vext#Extend(s:new_options, l:options)
  endif
  " add the features to thoses loaded.
  call vext#Extend(s:features, l:config)
endfunction

" Returns the value for the given path.
" The path can either be nothing, then the whole feature set will be returned,
" either just a path, and then the matching value will be returned, or if a
" list of paths or multiples paths is passed, then it will return a list of
" matching values.
function! self#features#Feature(...) abort

  if a:0 == 0
    return deepcopy(s:features)

  elseif a:0 == 1
    " if a list, call Feature on all the list's elements
    if type(a:1) == v:t_list
      let l:values = []
      for l:path in a:1
        call add(l:values, Feature(l:path))
      endfor
      return l:values
    endif

    " return the feature object
    if a:1 ==# '.'
      return deepcopy(s:features)
    endif
    " the real logic
    let l:paths = split(a:1, '\.')
    let l:tmp = deepcopy(s:features)
    for l:key in l:paths
      try
        let l:tmp = l:tmp[l:key]
      catch
        return v:null
      endtry
    endfor
    return l:tmp

  else " There is a multiple arguments, and call Feature on all of them
    let l:values = []
    for l:path in a:000
      call add(l:values, Feature(l:path))
    endfor
    return l:values
  endif

endfunction

" Returns true if the feature is present, and, if it's a boolean, if it's true
function! self#features#Has(featname) abort

  let l:paths = split(a:featname, '\.')
  let l:tmp = deepcopy(s:features)

  for l:key in l:paths
    try
      let l:tmp = l:tmp[l:key]
    catch
      return v:false
    endtry
  endfor

  if type(l:tmp) == v:t_bool
    return l:tmp
  elseif type(l:tmp) == type(v:null)
    return v:false
  else
    return v:true
  endif

endfunction

" Exists more for a debugging purpose than to have a feature.
function! self#features#GetOptions() abort
  return s:options
endfunction

" Exists more for a debugging purpose than to have a feature.
function! self#features#GetNewOptions() abort
  return s:new_options
endfunction

" Exists more for a debugging purpose than to have a feature.
function! self#features#GetFeatures() abort
  return s:features
endfunction

" Load the new options.
function! self#features#LoadOptions() abort
  call s:LoadOptions()
endfunction

function! s:LoadOptions() abort
  for l:key in keys(s:new_options)
    if s:CanLoadOption(l:key)
      call s:LoadOption(l:key, s:new_options[l:key])
    endif
    let s:options[l:key] = s:new_options[l:key]
    unlet s:new_options[l:key]
  endfor
endfunction

" TODO: add pattern matching for options?
function! s:CanLoadOption(optionName) abort
  let l:allowed_options = get(g:, 'allowed_options', [])
  return index(l:allowed_options, a:optionName) != -1
endfunction

function! s:LoadOption(optionName, optionValue) abort
  let l:typeVal = type(a:optionValue)
  let l:optionString = ''
  " value is an object
  if l:typeVal == v:t_dict
    try
      let l:value = a:optionValue.value
      if type(l:value) == v:t_list
        let l:value = join(l:value, ',')
      endif
      let l:mode = a:optionValue.mode
      if l:mode ==# 'add'
        let l:optionString = a:optionName . '+=' . l:value
      elseif l:mode ==# 'sub'
        let l:optionString = a:optionName . '-=' . l:value
      elseif l:mode ==# 'mul'
        let l:optionName = a:optionName . '^=' . l:value
      else
        call s:LogErr('option ' . a:optionName . ' uses a mode not supported: ' . a:optionValue.mode)
      endif
    catch /Vim(let):E716/
      call s:LogErr('option ' . a:optionName . ' does not have a standard architecture')
    endtry
  elseif l:typeVal == v:t_string || l:typeVal == v:t_number || l:typeVal == v:t_float
    let l:optionName = a:optionName . '=' . a:optionValue
  elseif l:typeVal == v:t_list
    let l:optionName = a:optionName . '=' . join(a:optionValue, ',')
  elseif l:typeVal == v:t_bool
    if a:optionValue
      let l:optionName = a:optionName
    else
      let l:optionName = 'no' . a:optionName
    endif
  endif
  try
    execute 'set ' . l:optionString
  catch /^Vim(execute):E/
    call s:LogErr('Failed to execute the following option: ' . l:optionString)
  endtry
endfunction

function! s:LogErr(msg) abort
  echohl WarningMsg | echomsg '[feat] ' . a:msg | echohl None
endfunction
