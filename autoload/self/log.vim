scriptencoding utf8

let g:self#log#lines = []

function! self#log#(...) abort
  for l:item in a:000
    call insert(g:self#log#lines, l:item, len(g:self#log#lines))
  endfor
endfunction
