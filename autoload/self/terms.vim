"=============================================================================
" FILE: terms.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

"
" Some utils for working with neovim terminal
"

if !has('nvim')
  finish
endif

function! self#terms#Split(func, count, mods) abort
  exe a:mods . ' ' . (a:count ? a:count : '') 'new'
  call a:func()
endfunction

function! self#terms#Run(cmd) abort
  if exists('b:terminal_job_id') && !exists('b:term_job_stopped')
    call jobstop(b:terminal_job_id)
    let b:term_job_stopped = 1
  endif
  exe 'terminal ' . a:cmd
  let b:term_command = a:cmd
  command! -buffer TAgain call self#terms#Run(b:term_command)
  nnoremap <buffer> <c-r> :TAgain
endfunction

function! self#terms#Bind(basecmd, args) abort
  let l:args = len(a:args) ? a:args : (exists('b:term_args') ? b:term_args : '')
  let l:full_cmd = a:basecmd . ' ' . l:args
  exe 'terminal ' . l:full_cmd
  let b:term_command = l:full_cmd
  let b:term_base_command = a:basecmd
  let b:term_args = l:args
  command! -buffer -nargs=* TAgain call self#terms#Bind(b:term_base_command, <q-args>)
  nnoremap <buffer> <c-r> :TAgain
endfunction
