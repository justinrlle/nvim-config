scriptencoding utf8

let g:self#config#global = {}
let g:self#config#workspace = {}
let s:workspace_config_path = findfile('.config/vim.json')
call self#log#([ 'workspace config path', s:workspace_config_path ])
