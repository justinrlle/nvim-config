"=============================================================================
" FILE: features.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

scriptencoding utf8

"
" Define functions used for nice status bars
"

let s:stls = {
      \ 'active': '',
      \ 'inactive': ''
      \ }

" symbols
let s:default_symbols = {
      \ 'readonly': '',
      \ 'modified': '+',
      \ 'unmodifiable': '-',
      \ 'branch': '',
      \ 'staged': '+',
      \ 'unstaged': '*'
      \ }

let s:symbols = get(g:, 'symbols', s:default_symbols)

function! self#status#Build(parts, symbols) abort

endfunction

function! self#status#StlSection(parts) abort

endfunction

function! self#status#Add(expr, ...) abort
  if a:0 == 0
    let s:stls.active .= a:expr
    let s:stls.inactive .= a:expr
  elseif a:1 ==? 'active'
    let s:stls.active .= a:expr
  elseif a:1 ==? 'inactive'
    let s:stls.inactive .= a:expr
  elseif a:1 ==? 'both'
    let s:stls.active .= a:expr
    let s:stls.inactive .= a:expr
  endif
endfunction

function! self#status#Get(type) abort
  return s:stls[a:type]
endfunction

function! self#status#Reset() abort
  let s:stls = {
        \ 'active': '',
        \ 'inactive': ''
        \ }
endfunction

function! self#status#StlExpr(expr, ...) abort
  if a:0 == 0
    return '%{match(' . a:expr . ', "^\s*$") == -1 ? ' . a:expr . ' . " " : "" }'
  endif
  return '%{match(' . a:expr . ', "^\s*$") == -1 ? ' . a:expr . '." ".' . a:1 . '. " " : "" }'
endfunction

function! self#status#StlMiddle() abort
  return '%='
endfunction

function! self#status#StlComponent(str) abort
  return ' ' . a:str
endfunction

function! self#status#StlColor(nb) abort
  return '%' . string(a:nb) . '*'
endfunction

function! s:readonly_f() abort
  return &filetype !~? 'help\|vimfiler\|gundo' && &readonly
endfunction

function! self#status#filepath() abort
  let l:path = expand('%:h')
  if l:path ==# '.'
    let l:path = expand('%:p:h:t')
  else
    let l:cwd = fnamemodify(getcwd(), ':p:h')
    if l:path == l:cwd
      let l:path = expand('%:p:h:t')
    elseif stridx(l:path, l:cwd) != -1
      let l:path = strcharpart(l:path, strlen(l:cwd) + 1)
      if get(g:, 'print', 0)
        echom 'path: ' . l:path
      endif
      if !isdirectory(l:path)
        let l:path = fnamemodify(l:path, ':h')
      endif
      if get(g:, 'print', 0)
        echom 'final path: ' . l:path
      endif
    endif
  endif
  return winwidth(0) > 70 ? l:path : ''
endfunction

function! self#status#readonly() abort
  return s:readonly_f() ? s:symbols.readonly : ''
endfunction

function! self#status#modified() abort
  return &filetype =~# 'help\|vimfiler\|gundo' ? '' : &modified ? s:symbols.modified : &modifiable ? '' : s:symbols.unmodifiable
endfunction

function! self#status#fileformat() abort
  if winwidth(0) > 70
    if self#HasFeat('plugins.devicons')
      return &fileformat . ' ' . WebDevIconsGetFileFormatSymbol()
    else
      return &fileformat
    endif
  endif
return ''
endfunction

function! self#status#filetype() abort
  if winwidth(0) > 70
    if self#HasFeat('plugins.devicons')
      return strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft'
    else
      return strlen(&filetype) ? &filetype : 'no ft'
    endif
  endif
  return ''
endfunction

function! self#status#fileencoding() abort
  let l:encoding = (&fileencoding !=# '' ? &fileencoding : &encoding)
  if l:encoding !=? 'utf-8'
    return winwidth(0) > 70 ? l:encoding : ''
  else
    return ''
  endif
endfunction

function! self#status#mode() abort
  let l:mode = mode(1)
  if l:mode ==# 'n'
    let l:mode = 'NORMAL'
  elseif l:mode ==# 'no'
    let l:mode = 'NORMAL'
  elseif l:mode ==# 'v'
    let l:mode = 'VISUAL'
  elseif l:mode ==# 'V'
    let l:mode = 'V-LINE'
  elseif l:mode ==# ''
    let l:mode = 'V-BLOCK'
  elseif l:mode ==# 's'
    let l:mode = 'SELECT'
  elseif l:mode ==# 'S'
    let l:mode = 'S-LINE'
  elseif l:mode ==# ''
    let l:mode = 'S-BLOCK'
  elseif l:mode ==# 'i'
    let l:mode = 'INSERT'
  elseif l:mode ==# 'R'
    let l:mode = 'REPLACE'
  elseif l:mode ==# 'Rv'
    let l:mode = 'VIRTUAL'
  elseif l:mode ==# 't'
    let l:mode = 'TERM'
  elseif l:mode ==# 'c'
    let l:mode = 'CMDLINE'
  elseif l:mode ==# 'cv'
    let l:mode = 'VIM-EX'
  elseif l:mode ==# 'ce'
    let l:mode = 'NORMAL-EX'
  elseif l:mode ==# 'r'
    let l:mode = 'WAITING...'
  elseif l:mode ==# 'rm'
    let l:mode = 'WAITING...'
  elseif l:mode ==# 'r?'
    let l:mode = 'CONFIRM'
  elseif l:mode ==# '!'
    let l:mode = 'SHELL'
  endif
  return winwidth(0) > 50 ? l:mode : ''
endfunction

function! self#status#fugitive() abort
  if exists('*fugitive#head') &&  winwidth(0) > 70
    let l:branch = fugitive#head()
    return l:branch !=# '' ? s:symbols.branch . ' ' . l:branch : ''
  endif
  return ''
endfunction

function! self#status#gina() abort
  " if exists('Gina') && winwidth(0) > 70
  if winwidth(0) > 70
    let l:branch = gina#component#repo#branch()
    if l:branch ==# ''
      return ''
    endif
    let l:status = s:symbols.branch . ' ' . l:branch
    let l:staged = gina#component#status#staged()
    let l:status = l:status . (l:staged ? ' ' . l:staged . s:symbols.staged : '')
    let l:unstaged = gina#component#status#unstaged()
    let l:status = l:status . (l:unstaged ? ' ' . l:unstaged . s:symbols.unstaged : '')
    return l:status
  endif
  return ''
endfunction

function! self#status#git() abort
  return self#status#gina()
endfunction

function! self#status#filename() abort
  return (s:readonly_f() ? self#status#readonly() . ' ' : '') .
        \ (&filetype ==# 'vimfiler' ? vimfiler#get_status_string() :
        \  &filetype ==# 'unite' ? unite#get_status_string() :
        \  &filetype ==# 'vimshell' ? vimshell#get_status_string() :
        \ '' !=# expand('%:t') ? expand('%:t') : '[No Name]') .
        \ ('' !=# self#status#modified() ? ' ' . self#status#modified() : '')
endfunction


function! s:LinterStatus() abort
    let l:counts = ale#statusline#Count(bufnr(''))

    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors
    let l:format = get(g:, 'ale_statusline_format', ['%dE %dW', 'OK'])

    return l:counts.total == 0 ? l:format[1] : printf(
    \   l:format[0],
    \   l:all_errors,
    \   l:all_non_errors,
    \)
endfunction

function! self#status#ale() abort
  return winwidth(0) > 70 && self#HasFeat('plugins.ale') ? s:LinterStatus() : ''
endfunction
