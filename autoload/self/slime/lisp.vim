"=============================================================================
" FILE: slime/lisp.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

function! self#slime#lisp#TopLevelDef() abort
  let l:pos = [line('.'), col('.')]
  let l:l_reg = @l
  normal! 100[("ly%
  let l:content = @l
  let @l = l:l_reg
  call cursor(l:pos)
  return l:content
endfunction
