"=============================================================================
" FILE: system.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

"
" Find the current OS, and allow to query it.
"

" TODO: add support for BSD (FreeBSD and OpenBSD)

let s:current_sys = ''

let g:self#system#systems = { 'linux': 'Linux', 'darwin': 'Darwin', 'win': 'Win' }

if has('win32') || has('win64')
  let s:current_sys = g:self#system#systems.win
else
  let g:system_name = substitute(system('uname -s'), '\n', '', '')
  if g:system_name ==? g:self#system#systems.linux
    let s:current_sys = g:self#system#systems.linux
  elseif g:system_name ==? g:self#system#systems.darwin
    let s:current_sys = g:self#system#systems.darwin
  endif
endif

function! self#system#system() abort
  return s:current_sys
endfunction

function! self#system#is_system(sysName) abort
  return s:current_sys ==? a:sysName
endfunction

function! self#system#is_darwin() abort
  return s:current_sys ==? g:self#system#systems.darwin
endfunction

function! self#system#is_win() abort
  return s:current_sys ==? g:self#system#systems.win
endfunction

function! self#system#is_linux() abort
  return s:current_sys ==? g:self#system#systems.linux
endfunction

function! self#system#is_unix() abort
  return self#system#is_darwin() || self#system#is_linux()
endfunction
