"=============================================================================
" FILE: ale.vim
" AUTHOR:  Justin Rerolle <justinrlle@pm.me>
" License: MIT license
"=============================================================================

" Configuration for the ale plugin for auto completion.

scriptencoding utf8

function! config#ale_completion#load() abort
  let g:ale_completion_enabled = 1
endfunction
