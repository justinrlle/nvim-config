"=============================================================================
" FILE: ale.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

" Configuration for the ale plugin.

scriptencoding utf8

let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_on_insert_leave = 1
let g:ale_sign_column_always = 1
let g:ale_sign_warning = '──'
let g:ale_sign_error = ''
let g:ale_statusline_format = [ '%d  %d ', '✓']
let g:ale_echo_msg_warning_str = 'warning'
let g:ale_echo_msg_error_str = 'error'
let g:ale_echo_msg_format = '%severity% (%linter%: %code%): %s'
let g:incs = ' -Isrc/bignum/ -Isrc/base -Isrc/AST -Itests '


" - linters -
let g:ale_linters = {}

function! s:define_compiler(lang, compiler, config) abort
  if executable(a:compiler)
    call add(g:ale_linters[a:lang], a:compiler)
    let l:compiler = substitute(a:compiler, '+', 'p', 'g')
    let l:includes = copy(a:config.includes)
    let l:system_includes = copy(a:config.system_includes)
    let l:includes = join(map(l:includes, '"-I " . v:val'), ' ') . ' '
          \ . join(map(l:system_includes, '"-isystem " . v:val'), ' ')
    execute 'let g:ale_' . a:lang . '_' . l:compiler . '_options = "'
          \ . a:config.flags . ' ' . l:includes . '"'
  endif
endfunction
" c
let g:ale_linters.c = []

" cpp
let g:ale_linters.cpp = []

" rust config
let g:ale_linters.rust = self#Feat('linters.rust', [])
" let g:ale_rust_ignore_error_codes = ['E0432', 'E0433']
let g:ale_rust_cargo_use_clippy = 1

" python
let g:ale_python_flake8_options = '--ignore E501'
let g:ale_linters.python = self#Feat('linters.python', ['flake8', 'mypy', 'pylint'])

function! config#ale#load() abort
  let s:c_config = {
        \ 'includes': [ 'src' ],
        \ 'system_includes': [ 'include' ],
        \ 'flags': '-Wall -Wextra -std=c99 -pedantic'
        \ }

  call s:define_compiler('c', 'gcc', s:c_config)
  call s:define_compiler('c', 'clang', s:c_config)
  call s:define_compiler('c', 'cppcheck', s:c_config)
  let s:cpp_config = {
        \ 'includes': [ 'src' ],
        \ 'system_includes': [ 'include' ],
        \ 'flags': '-Wall -Wextra -std=c++14 -pedantic'
        \ }
  call s:define_compiler('cpp', 'g++', s:cpp_config)
  call s:define_compiler('cpp', 'clang', s:cpp_config)
  call s:define_compiler('cpp', 'cppcheck', s:cpp_config)

  " - mappings -
  nnoremap <silent> <leader>an :ALENextWrap<CR>
  nnoremap <silent> <leader>ap :ALEPreviousWrap<CR>
  nnoremap <silent> <leader>al :ALELast<CR>
  nnoremap <silent> <leader>af :ALEFirst<CR>
  inoremap <c-l> <c-o>:ALELint<CR>
endfunction
