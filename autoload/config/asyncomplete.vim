"=============================================================================
" FILE: asyncomplete.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

" Configuration for the asyncomplete plugin.

scriptencoding utf8

let g:asyncomplete_remove_duplicates = 1
let g:asyncomplete_smart_completion = 1
let g:asyncomplete_auto_popup = 0
let s:cache_dir = expand(self#Feat('paths.cache', '~'))
if filewritable(s:cache_dir . '/asyncomplete.log') || filewritable(s:cache_dir)
  let g:asyncomplete_log_file = s:cache_dir . '/asyncomplete.log'
endif

" some functions to ease registering sources
function! config#asyncomplete#register(src, opts) abort
  let l:GetSources = function('asyncomplete#sources#' . a:src . '#get_source_options')
  let l:Completor = function('asyncomplete#sources#' . a:src . '#completor')
  let l:opts = vext#Extend({ 'completor': l:Completor, 'name': a:src }, a:opts)
  return asyncomplete#register_source(l:GetSources(l:opts))
endfunction

function! config#asyncomplete#load() abort
  augroup Asyncomplete
    au!

    autocmd User asyncomplete_setup call config#asyncomplete#register('omni', { 'whitelist': ['*'] })
    " autocmd User asyncomplete_setup call config#asyncomplete#register('buffer', { 'whitelist': ['*'], 'priority': -1})
    autocmd User asyncomplete_setup call config#asyncomplete#register('file', { 'priority': 5, 'whitelist': ['*'] })
    autocmd User asyncomplete_setup call config#asyncomplete#register('necovim', { 'whitelist': ['vim'] })
    " TODO: tags
  augroup END
endfunction


