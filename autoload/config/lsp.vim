"=============================================================================
" FILE: asyncomplete.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

" Configuration for the lsp plugin.

scriptencoding utf8

let g:lsp_signs_enabled = 1
let g:lsp_diagnostics_echo_cursor = 1
let g:lsp_signs_error = {'text': 'X'}
let g:lsp_signs_warning = {'text': '-'}
let g:lsp_signs_hint = {'text': '|'}

let s:cache_dir = expand(self#Feat('paths.cache', '~'))
" lsp settings
if filewritable(s:cache_dir . '/lsp.log') || filewritable(s:cache_dir)
  let g:lsp_log_file = s:cache_dir . '/lsp.log'
  let g:lsp_log_verbose = 1
endif
" shorter function name
function! s:find_nearest(filename) abort
  let l:uri = lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), a:filename))
  return l:uri
endfunction

" You always want shell and shellcmdflag.
function! s:wrap_exec(args) abort
  let l:args = [&shell, &shellcmdflag]
  call extend(l:args, a:args)
  return l:args
endfunction


function! config#lsp#register(opts) abort
  let l:lsp_opts = { 'name': a:opts.name }
  if type(a:opts.cmd) == v:t_func
    let l:lsp_opts.cmd = a:opts.cmd
  else
    let l:lsp_opts.cmd = { server -> s:wrap_exec(a:opts.cmd) }
  endif
  if has_key(a:opts, 'root_uri')
    if type(a:opts.root_uri) == v:t_func
      let l:lsp_opts.root_uri = a:opts.root_uri
    else
      let l:lsp_opts.root_uri = { server -> s:find_nearest(a:opts.root_uri) }
    endif
  endif
  if has_key(a:opts, 'whitelist')
    let l:lsp_opts.whitelist = a:opts.whitelist
  endif
  if has_key(a:opts, 'blacklist')
    let l:lsp_opts.blacklist = a:opts.blacklist
  endif
  if has_key(a:opts, 'config')
    let l:lsp_opts.config = a:opts.config
  endif
  if has_key(a:opts, 'workspace_config')
    let l:lsp_opts.workspace_config = a:opts.workspace_config
  else
    let l:lsp_opts.workspace_config = get(g:workspace_config, a:opts.name, {})
    call self#log#({'lsp config': a:opts.name, 'workspace_config': l:lsp_opts.workspace_config})
  endif
  call lsp#register_server(l:lsp_opts)
endfunction

function! config#lsp#load() abort
  augroup Lsp
    au!

    if executable('rustup') && executable('rls')
      let s:rls_cmd = ''
      if self#system#is_win()
        let s:rls_cmd = 'set RUST_BACKTRACE=1 && rls'
      else
        let s:rls_cmd = 'RUST_BACKTRACE=1 rls'
      endif
      autocmd User lsp_setup call config#lsp#register({
            \ 'name': 'rls',
            \ 'cmd': [s:rls_cmd],
            \ 'root_uri': 'Cargo.toml',
            \ 'whitelist': ['rust'],
            \ })
    endif

    if executable('hie')
      autocmd User lsp_setup call config#lsp#register({
            \ 'name': 'hie',
            \ 'cmd': ['hie', '--lsp'],
            \ 'whitelist': ['haskell']
            \ })
    endif

    if executable('pyls')
      autocmd User lsp_setup call config#lsp#register({
            \ 'name': 'pyls',
            \ 'cmd': ['pyls'],
            \ 'whitelist': ['python']
            \ })
    endif

    let s:clangd_executable = self#Feat('paths.clang', '') . '/bin/clangd'
    if executable(s:clangd_executable)
      autocmd User lsp_setup call config#lsp#register({
            \ 'name': 'clangd',
            \ 'cmd': [s:clangd_executable],
            \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
            \ })
    endif

    if executable('flow-language-server')
      autocmd User lsp_setup call config#lsp#register({
            \ 'name': 'flow-language-server',
            \ 'cmd': ['flow-language-server --stdio'],
            \ 'root_uri': '.flowconfig',
            \ 'whitelist': ['javascript'],
            \ })
    endif

    if executable('typescript-language-server') && executable('tsserver')
      autocmd User lsp_setup call config#lsp#register({
            \ 'name': 'typescript-language-server',
            \ 'cmd': ['typescript-language-server --stdio'],
            \ 'root_uri': 'tsconfig.json',
            \ 'whitelist': ['typescript'],
            \ })
    endif
  augroup END
endfunction
