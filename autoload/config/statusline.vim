"=============================================================================
" FILE: statusline.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

scriptencoding utf8

" Configuration for the statusline.

let g:stl_seps = {
      \ 'left': '',
      \ 'right': ''
      \ }


function! SetupColors() abort
  hi User1 guifg=#AABBC4 guibg=#282828 ctermfg=250 ctermbg=235
  hi User2 guifg=#658595 guibg=#282828 ctermfg=245 ctermbg=235
  hi User3 guifg=#475C69 guibg=#282828 ctermfg=49 ctermbg=235
  hi StatusLineNC gui=none guibg=#282828 ctermbg=235
  hi StatusLine gui=none guibg=#282828 ctermbg=235
endfunction

function! config#statusline#load() abort

  call SetupColors()

  call self#status#Reset()
  call self#status#Add(self#status#StlColor(1), 'active')
  call self#status#Add(' ', 'active')
  call self#status#Add(self#status#StlExpr('self#status#mode()'), 'active')
  call self#status#Add(self#status#StlColor(2))
  call self#status#Add(' ', 'inactive')
  call self#status#Add(self#status#StlExpr('g:stl_seps.left'), 'active')
  call self#status#Add(self#status#StlExpr('self#status#git()', 'g:stl_seps.left'), 'active')
  call self#status#Add(self#status#StlExpr('self#status#filename()'))
  call self#status#Add(self#status#StlColor(3), 'inactive')
  call self#status#Add(self#status#StlMiddle())

  if self#HasFeat('plugins.ale')
    call self#status#Add(self#status#StlExpr('self#status#ale()', 'g:stl_seps.right'))
  endif

  call self#status#Add(self#status#StlExpr('self#status#filetype()', 'g:stl_seps.right'))
  call self#status#Add('%-3(%p%%%) %-7(%l:%v%)')

  let g:active_stl = self#status#Get('active')
  let g:inactive_stl = self#status#Get('inactive')
  let &statusline = g:active_stl

  augroup StatusLine
    autocmd!
    autocmd WinEnter * setlocal statusline=%!g:active_stl
    autocmd WinLeave * setlocal statusline=%!g:inactive_stl
  augroup END
endfunction
