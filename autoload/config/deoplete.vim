"=============================================================================
" FILE: deoplete.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

" Configuration for the deoplete plugin.

scriptencoding utf8

function! config#deoplete#load() abort
  call deoplete#enable()
  let s:clang = self#Feat('paths.clang', '/usr') . '/bin/clang'
  if strlen(s:clang) && executable(s:clang)
    let g:deoplete#sources#clang#executable = s:clang
  endif
  let g:deoplete#sources#clang#std = { 'c': 'c99', 'cpp': 'c++14' }
  let g:deoplete#sources#clang#flags = [ '-Iinclude', '-Isrc' ]
endfunction
