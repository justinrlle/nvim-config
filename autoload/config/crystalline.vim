scriptencoding utf8

let g:crystalline_separators = ['', '']


function! config#crystalline#StatusLine(current, width) abort
  let l:s = ''

  if a:current
    let l:s .= crystalline#mode() . crystalline#right_mode_sep('Fill')
    let l:s .= ' %{self#status#git()} ' . crystalline#right_sep('', 'Fill')
  else
    let l:s .= '%#CrystallineInactive#'
  endif

  let l:s .= ' %{self#status#filename()} %h%w'
  let l:s .= '%='

  if a:current
    let l:s .= '%{&spell ? "SPELL " : ""} ' . crystalline#left_sep('', 'Fill')
  else
    let l:s .= '%#CrystallineInactive#'
  endif
  if a:width > 80
    if a:current && self#HasFeat('plugins.ale')
      let l:s .= ' %{self#status#ale()} ' . crystalline#left_sep('', 'Fill') 
    endif
    let l:s .= ' %{self#status#filetype()}[%{&enc}][%{&ffs}] ' . crystalline#left_sep('', 'Fill')
  endif

  let l:s .= ' %-3(%p%%%) %-7(%l:%v%)'

  return l:s
endfunction

function! config#crystalline#load() abort
  let g:crystalline_enable_sep = 1
  let g:crystalline_statusline_fn = 'config#crystalline#StatusLine'

  set laststatus=2
endfunction

