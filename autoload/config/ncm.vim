"=============================================================================
" FILE: nvim-cm.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

" Configuration for the nvim-cm plugin.

" config
let s:clang = self#Feat('paths.clang', '/usr/')
let g:clang_library_path = s:clang . '/lib'

" lsp config
let g:LanguageClient_serverCommands = {}

if executable('rls')
  let g:LanguageClient_serverCommands.rust = [ 'rls' ]
endif

if executable('pyls')
  let g:LanguageClient_serverCommands.python = [ 'pyls' ]
endif

let s:clangd_executable = s:clang . '/bin/clangd'
if executable(s:clangd_executable)
  let g:LanguageClient_serverCommands.c = [ s:clangd_executable ]
  let g:LanguageClient_serverCommands.cpp = [ s:clangd_executable ]
  let g:LanguageClient_serverCommands.objc = [ s:clangd_executable ]
  let g:LanguageClient_serverCommands.objcpp = [ s:clangd_executable ]
endif

if executable('flow-language-server')
  let g:LanguageClient_serverCommands.javascript = [ 'flow-language-server', '--stdio' ]
endif

if executable('typescript-language-server')
  let g:LanguageClient_serverCommands.typescript = [ 'typescript-language-server', '--stdio' ]
endif


function! config#ncm#load() abort

endfunction
