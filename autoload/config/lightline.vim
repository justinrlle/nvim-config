"=============================================================================
" FILE: lightline.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

scriptencoding utf8

" Configuration for the lightline plugin

let g:lightline = {
      \   'colorscheme': 'neodark',
      \   'separator': { 'left': '', 'right': '' },
      \   'subseparator': { 'left': '', 'right': '' },
      \   'component_function': {
      \       'modified': 'self#status#modified',
      \       'readonly': 'self#status#readonly',
      \       'git': 'self#status#git',
      \       'filename': 'self#status#filename',
      \       'fileformat': 'self#status#fileformat',
      \       'filetype': 'self#status#filetype',
      \       'fileencoding': 'self#status#fileencoding',
      \       'mode': 'self#status#mode',
      \       'filepath': 'self#status#filepath',
      \   },
      \   'active': {
      \   'left': [ [ 'mode', 'paste'],
      \             [ 'git', 'filename' ] ],
      \   'right': [ [ 'lineinfo' ],
      \              [ 'filepath', 'percent' ],
      \              [ 'fileencoding', 'filetype' ] ]
      \   },
      \   'inactive': {
      \   'left': [ [ 'filename' ] ],
      \   'right': [ [ 'lineinfo' ], [ 'filepath', 'percent' ] ]
      \   }
      \ }

function! config#lightline#load() abort

  if self#HasFeat('plugins.ale')

    let g:lightline.component_function.ale = 'self#status#ale'
    let g:lightline.active.right[2] = [ 'ale' ] + g:lightline.active.right[2]
  endif

endfunction
