"=============================================================================
" FILE: paths.vim
" AUTHOR:  Justin Rerolle <justin.rerolle@protonmail.com>
" License: MIT license
"=============================================================================

"
" Contains all the paths for the installation.
"

" The exported paths
let g:paths#config_dir = g:init_path
let g:paths#cache = ''
let g:paths#dein = ''
let g:paths#plugins = ''

" Default paths
let s:defaults = {}
let s:defaults.cache = {
      \ 'win': expand('~') . '/AppData/Local/nvim',
      \ 'unix': expand('~') . '/.cache/nvim'
      \ }
let s:defaults.cache.current = has('win32')
      \ ? s:defaults.cache.win
      \ : s:defaults.cache.unix

" Just the suffix of the paths
let s:defaults.plugins = 'dein'
let s:defaults.dein = 'repos/github.com/Shougo/dein.vim'

let s:p_cache = expand(self#Feat('paths.cache', s:defaults.cache.current))
let s:p_plugins = self#Feat('paths.plugins', s:defaults.plugins)
if s:p_plugins[0] !=# '/' && s:p_plugins[0] !=# '~'
  let s:p_plugins = expand(s:p_cache . '/' . s:p_plugins)
endif

let g:paths#cache = s:p_cache
let g:paths#plugins = s:p_plugins
let g:paths#dein = expand(s:p_plugins . '/' . s:defaults.dein)
